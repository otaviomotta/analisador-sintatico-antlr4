package otavio.org.example;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.List;
public class PascalCompiler {
    private String codigoCompilar;

    public PascalCompiler(String codigoCompilar) {
        this.codigoCompilar = codigoCompilar;
    }

    public void compile() {
        CharStream input = CharStreams.fromString(this.codigoCompilar);
        OM_Gramatica_PascalLexer lexer = new OM_Gramatica_PascalLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        tokens.fill();
        List<Token> tokenList = tokens.getTokens();

        for (Token token : tokenList) {
            int tokenType = token.getType();
            String tokenName = lexer.getVocabulary().getSymbolicName(tokenType);
            String tokenText = token.getText();
            int line = token.getLine();
            int column = token.getCharPositionInLine();
            System.out.println("Token: " + tokenName + " | Texto: " + tokenText + " | Linha: " + line + " | Coluna: " + column);
        }
        OM_Gramatica_PascalParser parser = new OM_Gramatica_PascalParser(tokens);
        ParseTree tree = parser.program();
    }
}
